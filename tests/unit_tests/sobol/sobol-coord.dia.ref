// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.
// 
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// See the -coordinate option in action.
// Show how to get the 12-th coordinate of the
// Sobol sequence.
lds = lowdisc_new("sobol");
lds = lowdisc_configure(lds,"-dimension",12);
lds = lowdisc_configure(lds,"-coordinate",%t);
// Elements #1,...,#5, coordinate index = 12.
[lds,computed1] = lowdisc_next (lds,5);
// Elements #6,...,#10, coordinate index = 12.
[lds,computed2] = lowdisc_next (lds,5);
lds = lowdisc_destroy(lds);
//
// Reference : 12-th coordinate of dimension 12 Sobol
lds = lowdisc_new("sobol");
lds = lowdisc_configure(lds,"-dimension",12);
[lds,expected] = lowdisc_next (lds,10);
lds = lowdisc_destroy(lds);
expected=expected(:,12)
expected  = 
    0.5
    0.75
    0.25
    0.625
    0.125
    0.375
    0.875
    0.8125
    0.3125
    0.0625
//
// Tests
assert_checkequal(computed1,expected(1:5));
assert_checkequal(computed2,expected(6:10));
//////////////////////////////////////////
//
// Scrambled Sobol sequence.
lds = lowdisc_new("sobol");
lds = lowdisc_configure(lds,"-dimension",12);
lds = lowdisc_configure(lds,"-scrambling","Owen");
lds = lowdisc_configure(lds,"-coordinate",%t);
// Elements #1,...,#5, coordinate index = 12.
[lds,computed1] = lowdisc_next (lds,5);
// Elements #6,...,#10, coordinate index = 12.
[lds,computed2] = lowdisc_next (lds,5);
lds = lowdisc_destroy(lds);
//
// Reference : 12-th coordinate of dimension 12 Sobol
lds = lowdisc_new("sobol");
lds = lowdisc_configure(lds,"-dimension",12);
lds = lowdisc_configure(lds,"-scrambling","Owen");
[lds,expected] = lowdisc_next (lds,10);
lds = lowdisc_destroy(lds);
expected=expected(:,12)
expected  = 
    0.079142
    0.7650172
    0.6640723
    0.4906337
    0.9709188
    0.1839093
    0.2765522
    0.5677289
    0.7167606
    0.3785258
//
// Tests
assert_checkequal(computed1,expected(1:5));
assert_checkequal(computed2,expected(6:10));
////////////////////////////////////////////
//
// Performance test
N=10000;
tic();
for i=1:10
    dimension=2^i;
    lds = lowdisc_new("sobol");
    lds = lowdisc_configure(lds,"-dimension",dimension);
    lds = lowdisc_configure(lds,"-coordinate",%t);
    [lds,computed1] = lowdisc_next (lds,N);
    lds = lowdisc_destroy(lds);
end
t=toc();
assert_checktrue(t<10);
////////////////////////////////////////////
//
// Performance test
N=10000;
tic();
for dimension=1:4:40
    lds = lowdisc_new("sobol");
    lds = lowdisc_configure(lds,"-dimension",dimension);
    lds = lowdisc_configure(lds,"-scrambling","Owen");
    lds = lowdisc_configure(lds,"-coordinate",%t);
    [lds,computed1] = lowdisc_next (lds,N);
    lds = lowdisc_destroy(lds);
end
t=toc();
assert_checktrue(t<10);
