// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009-2011 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the 
// GNU LGPL license.
// 

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

//
// Check the Fast Niederreiter sequence 
// Use base 2 - this is the default.
//
lds = lowdisc_new("niederreiter");
lds = lowdisc_configure(lds,"-dimension",4);
// Term #1
[lds,computed] = lowdisc_next (lds);
expected = [0.500000      0.500000      0.750000      0.875000];
assert_checkalmostequal ( computed, expected , 10 * %eps );
// Terms #2 to #9
[lds,computed]=lowdisc_next(lds,8);
expected= [...
  0.250000      0.750000      0.562500      0.765625
  0.750000      0.250000      0.312500      0.140625
  0.125000      0.625000      0.437500      0.546875
  0.625000      0.125000      0.687500      0.421875
  0.375000      0.375000      0.875000      0.281250
  0.875000      0.875000      0.125000      0.656250
  0.062500      0.937500      0.953125      0.234375
  0.562500      0.437500      0.203125      0.859375
];
assert_checkalmostequal ( computed, expected , 1.e-4 );
lds = lowdisc_destroy(lds);


