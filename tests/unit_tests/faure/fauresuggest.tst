// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009-2011 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the GNU LGPL license.
// 

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// See the minimum number of simulations for integration in dimension 4.
dim = 4;
base = 5;
// Taken from "Algorithm 659", p97, Table II
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base );
assert_checkequal ( [nsim,skip,leap] , [125 623 0] );
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base , 10000);
assert_checkequal ( [nsim,skip,leap] , [15625 623 0] );
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base , 50000);
assert_checkequal ( [nsim,skip,leap] , [78125 623 0] );
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base , 100000);
assert_checkequal ( [nsim,skip,leap] , [390625 623 0] );

// Check that nsim >= nsimmin for several powers of 10
dim = 4;
for nsimmin = logspace(1,10,10)
  [nsim,skip,leap] = lowdisc_fauresuggest ( dim , nsimmin );
  assert_checkequal ( nsim >= nsimmin , %t );
end


// Use the minimum number of simulations for integration in dimension 4.
dim = 4;
lds = lowdisc_new("faure");
lds = lowdisc_configure(lds,"-dimension",dim);
base = lowdisc_get(lds,"-faureprime");
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base );
assert_checkequal ( [nsim,skip,leap] , [125 623 0] );
lds = lowdisc_configure(lds,"-skip",skip);
lds = lowdisc_configure(lds,"-leap",leap);

[lds,experiments]=lowdisc_next(lds,nsim);
lds = lowdisc_destroy(lds);


// Use the minimum number of simulations for integration in dimension 4.
dim = 4;
nsimmin = 1000;
lds = lowdisc_new("faure");
lds = lowdisc_configure(lds,"-dimension",dim);
base = lowdisc_get(lds,"-faureprime");
[nsim,skip,leap] = lowdisc_fauresuggest ( dim , base , nsimmin );
assert_checkequal ( [nsim,skip,leap] , [3125 623 0] );
lds = lowdisc_configure(lds,"-skip",skip);
lds = lowdisc_configure(lds,"-leap",leap);

[lds,experiments]=lowdisc_next(lds,nsim);
lds = lowdisc_destroy(lds);
