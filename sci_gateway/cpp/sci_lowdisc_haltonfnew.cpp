// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
//#include "stack-c.h" 
#include "Scierror.h"
#include "localization.h"
#include "liblowdiscgateway.h"
#include "api_scilab.h"
}

/* ==================================================================== */

#include "gw_lowdisc_support.h" 
#include "lowdisc_math.h" 
#include "halton.h" 
#include "lowdisc_halton_map.hxx" 


// token=_lowdisc_haltonfnew(dim, base, seed, scrambling, coordinate)
//   Start the Halton sequence.
//
// Parameters
//   dim : the number of dimensions (e.g. 1)
//   base : a 1 x dim matrix of doubles, 
//      a list of successive prime numbers (e.g. (0,0,0,0) for automatic 
//      setup or (2,3,5,7) for user-defined setup)
//      If base(i) = 0 then the prime number #i is automatically set.
//	    If base(i) > 1 then the base is directly used.
//	    If base(i) <0 or base(i) = 1, this is an error.
//   seed : a 1 x dim matrix of doubles, the Halton sequence element corresponding 
//      to index = 0 (e.g. (0,0,0,0) for default setup)
//   scrambling : a 1x1 matrix of doubles, the scrambling. scrambling=1 for no scrambling, 
//      scrambling=2 for RR2 (Kocis-Whiten) scrambling,
//      scrambling=3 for Reverse (Vandewoestyne and Cools) scrambling
//   coordinate: a 1-by-1 matrix of boolean, 
//      If false, we must generate all coordinates.
//      If true, we must generate only the dimension-th coordinate.
//
// Description
// Internally uses leap = (1,1, ..., 1) for the halton C library.

int sci_lowdisc_haltonfnew (char *fname, void* pvApiCtx) {
	int dim;
	int nRows;
	int nCols;
	int ierr;
	double *dbase = NULL;
	int * base = NULL;
	double *dseed = NULL;
	int * seed = NULL;
	int * leap = NULL;
	int scrambling = NULL;
	Halton * seq;
	int token;
	int coordinate;
	
	CheckRhs(5,5) ;
	CheckLhs(0,1) ;
	//
	// Get Arg #1: dim
	ierr = lowdisc_GetOneIntegerArgument ( fname , 1 , &dim, pvApiCtx );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//
	// Get Arg #2: base
	ierr = lowdisc_AssertVariableType(fname , 2 , sci_matrix, pvApiCtx);
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//SciError sciError;	
	int *piAddr;
		
	getVarAddressFromPosition(pvApiCtx , 2, &piAddr);
	getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols, &dbase);
	
	//GetRhsVarMatrixDouble(pvApiCtx , 2 , &nRows,&nCols, &dbase);

	ierr = lowdisc_AssertNumberOfRows ( fname , 2 , 1 , nRows );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	ierr = lowdisc_AssertNumberOfColumns ( fname , 2 , dim , nCols );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	// Transfer the double array into an array of integers
	base = ivector ( dim );
	for(int k = 0; k < dim; k++) {
		ierr = lowdisc_Double2IntegerArgument ( fname , 2 , dbase[k] , base+k );
		if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
			return 0;
		}
	}
	//
	// Get Arg #3: seed
	ierr = lowdisc_AssertVariableType ( fname , 3 , sci_matrix, pvApiCtx );
	if ( ierr == LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//int * &piAddr2;
	getVarAddressFromPosition(pvApiCtx , 3, &piAddr);
	getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols, &dseed);

	//GetRhsVarMatrixDouble ( 3 , &nRows, &nCols, &dseed);
	ierr = lowdisc_AssertNumberOfRows ( fname , 3 , 1 , nRows );
	if ( ierr == LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	ierr = lowdisc_AssertNumberOfColumns ( fname , 3 , dim , nCols );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	// Transfer the double array into an array of integers
	seed = ivector ( dim );
	for(int k = 0; k < dim; k++) {
		ierr = lowdisc_Double2IntegerArgument ( fname , 3 , dseed[k] , seed+k );
		if ( ierr == 0 ) {
			return 0;
		}
	}
	//
	// Get Arg #4: scrambling
	ierr = lowdisc_GetOneIntegerArgument ( fname , 4 , &scrambling, pvApiCtx);
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//
	// Get Arg #5: coordinate (coordinate=1 if false).
	ierr = lowdisc_GetOneBooleanArgument ( fname , 5, &coordinate,pvApiCtx);
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	// Create the leap vector
	leap = ivector ( dim );
	for(int k = 0; k < dim; k++) {
		leap[k]=1;
	}
	//
	// Start the Halton sequence
	seq = new Halton ( dim , base , seed, leap , scrambling, coordinate);
	token = lowdisc_halton_map_add(seq);
	//
	// Free the vectors
	free_ivector ( base );
	free_ivector ( seed );
	free_ivector ( leap );
	//
	lowdisc_CreateLhsInteger ( 1 , token, pvApiCtx );
	return 0;
}
